<?php
$nombre = filter_input(INPUT_POST, 'name');
$correo = filter_input(INPUT_POST, 'email');
$mensaje = filter_input(INPUT_POST, 'message');

$body = '<strong>Nombre:</strong>  '.$nombre.'<br/>';
$body .= '<strong>Correo:</strong>  '.$correo.'<br/>';
$body .= '<strong>Mensaje:</strong>  '.$mensaje.'<br/>';

// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

//Load Composer's autoloader
require './vendor/autoload.php';

$mail = new PHPMailer(true);                              // Passing `true` enables exceptions
try {
    //Server settings
    $mail->SMTPDebug = 0;                                 // Enable verbose debug output
    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = 'smtp.ionos.mx';  // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = 'noreply@guillermocaflo.com.mx';                 // SMTP username
    $mail->Password = 'r6dUpr$k*s';                           // SMTP password
    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 587;                                    // TCP port to connect to

    //Recipients
    //$mail->setFrom('no-reply@guillermocaflo.com.mx', 'Web GuillermoCaflo');
    $mail->setFrom($correo, $nombre);
    $mail->addAddress('guillermocaflo@gmail.com', 'Guillermo Cabrera');     // Add a recipient

    //Attachments
    //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
    //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

    //Content
    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->Subject = 'Contacto Web '.date('Y-m-d H:i:s');
    $mail->Body    = $body;
    $mail->send();

    echo 'Gracias, por ponerte en contacto conmigo.';
} catch (Exception $e) {
    echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
}
