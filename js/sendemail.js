function validateForm(){
    "use strict";
    var title = $("#name").val();
    var err=true;
    if (title=="" || title==null) {
        $("#name").addClass("validation");
        var err= false;
    } else {
        $("#name").removeClass("validation");
        $("#name").addClass("form-control");
    }

    var email = $("#email").val();
    if (!(/(.+)@(.+){2,}\.(.+){2,}/.test(email))) {
        $("#email").addClass("validation");
        var err= false;
    } else {
        $("#email").removeClass("validation");
        $("#email").addClass("form-control");
    }

    var title = $("#message").val();
    if (title=="" || title==null) {
        $("#message").addClass("validation");
        var err= false;
    } else{
        $("#message").removeClass("validation");
        $("#message").addClass("form-control");
    }
    return err;
}

$(document).ready(function(){
    "use strict";
    $("#button").click(function(e){
        var isValid = validateForm();
        console.log(isValid);
        if(isValid){
            e.preventDefault();
            $.ajax({
                type: "POST",
                url: "send_mail.php",
                data:$("#ContactForm").serialize(),
                success:function(result){
                    $("#successmsg").html(result);
                }
            });
            $("#name").val('');
            $("#email").val('');
            $("#message").val('');
            // $("#successmsg").remove();
        }
        else{
            return false;
        }
    });
});
